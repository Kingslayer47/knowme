from re import search
from flask import Flask, redirect, url_for, render_template, request , session , flash, Blueprint, send_from_directory
from flask import jsonify
from flask_wtf import FlaskForm
from pkg_resources import AvailableDistributions
from wtforms import StringField, PasswordField
import json, binascii
from flask_mail import Mail, Message
from datetime import timedelta
import datetime
from passlib.hash import sha256_crypt
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Integer, String, Boolean
from sqlalchemy.orm import Mapped, mapped_column
import hashlib 
import logging
from subprocess import call
import os
from dotenv import load_dotenv
import random
import string
import requests
import shutil
import hmac
from werkzeug.utils import secure_filename
from flask_uploads import UploadSet , IMAGES , configure_uploads

load_dotenv('.env')
pictures = UploadSet('photos',IMAGES)

UPLOAD_FOLDER = os.getenv('UPLOAD_PATH')
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif', 'pdf'}

app = Flask(__name__, template_folder="Template")


app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///webapp.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = os.getenv('APP_SECRET')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['UPLOADED_PHOTOS_DEST'] = UPLOAD_FOLDER

app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = os.getenv('EMAIL')
app.config['MAIL_PASSWORD'] = os.getenv('password')

mail = Mail(app)

database = SQLAlchemy()
database.init_app(app)


configure_uploads(app, pictures)
app.permanent_session_lifetime = timedelta(days=14)

def igen(length): #Genrates Random Codes
    letters_and_digits = string.ascii_letters + string.digits
    genid = ''.join((random.choice(letters_and_digits) for i in range(length)))
    return genid

class users(database.Model):
    num: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    name: Mapped[str] = mapped_column(String)
    username: Mapped[str] = mapped_column(String)
    email: Mapped[str] = mapped_column(String)
    password: Mapped[str] = mapped_column(String)
    verifyid: Mapped[str] = mapped_column(String, default=igen(32))
    userid: Mapped[str] = mapped_column(String)
    verified: Mapped[bool] = mapped_column(Boolean,default=False)
    title: Mapped[str] = mapped_column(String,default="KnowMe User")

class info(database.Model):
    num: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    userid: Mapped[str] = mapped_column(String)
    name: Mapped[str] = mapped_column(String)
    city: Mapped[str] = mapped_column(String)
    state: Mapped[str] = mapped_column(String)
    country: Mapped[str] = mapped_column(String)
    bio: Mapped[str] = mapped_column(String)
    skills: Mapped[str] = mapped_column(String)
    dp: Mapped[str] = mapped_column(String,default="default_avatar.png")
    cover: Mapped[str] = mapped_column(String,default="original.png")
    title: Mapped[str] = mapped_column(String,default="KnowMe User")

class certs(database.Model):
    num: Mapped[int] = mapped_column(Integer,primary_key=True)
    userid: Mapped[str] = mapped_column(String)
    certificate: Mapped[str] = mapped_column(String)
    name: Mapped[str] = mapped_column(String)
    file_name: Mapped[str] = mapped_column(String)
    about: Mapped[str] = mapped_column(String)

class sacc(database.Model):
    num : Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    userid : Mapped[str] = mapped_column(String)
    gitlab : Mapped[str] = mapped_column(String,default=None)
    github : Mapped[str] = mapped_column(String,default=None)
    twitter : Mapped[str] = mapped_column(String,default=None)
    facebook : Mapped[str] = mapped_column(String,default=None)
    linkedin : Mapped[str] = mapped_column(String,default=None)
    instagram : Mapped[str] = mapped_column(String,default=None)


class blogPost(database.Model):
    num: Mapped[str] = mapped_column(Integer,primary_key=True)
    userid: Mapped[str] = mapped_column(String)
    url: Mapped[str] = mapped_column(String,default=None)
    name: Mapped[str] = mapped_column(String,default=None)
    about: Mapped[str] = mapped_column(String,default=None)
    blogid: Mapped[str] = mapped_column(String,default=igen(64))
    blogImg: Mapped[str] = mapped_column(String,default="blogDefault.svg")
    
class alerts(database.Model):
    num: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    userid: Mapped[str] = mapped_column(String)
    unique_id: Mapped[str] = mapped_column(String, default=igen((64)))
    content: Mapped[str] = mapped_column(String,default=None)
    author: Mapped[str] = mapped_column(String,default=None)
    verified_author: Mapped[str] = mapped_column(String,default=None)
    time_sent: Mapped[str] = mapped_column(String, default=datetime.datetime.now())



class verification_links():
    sent_link = {}

class user_configs():

    user_objs = {}

    def __init__(self,sess,theme="dark"):
        self.user = session[sess]
        self.theme = theme
        self.user_objs[session[sess]] = self 

def system_alert(userid, message,author="System", verified_author=True):
    new_alert = alerts(userid=userid, content=message, author=author,verified_author=verified_author)
    database.session.add(new_alert)
    database.session.commit()

def color():
	color_list = ['#FF355E','#FD5B78','#FF6037','#FF9966','#FF9933','#FFCC33','#FFFF66','#FFFF66','#CCFF00','#66FF66','#AAF0D1','#50BFE6','#FF6EFF','#EE34D2','#FF00CC','#FF00CC','#FF3855','#FD3A4A','#FB4D46','#FA5B3D','#FFAA1D','#FFF700','#299617','#A7F432','#2243B6','#5DADEC','#5946B2','#9C51B6','#A83731','#AF6E4D','#BFAFB2','#FF5470','#FFDB00','#FF7A00','#FDFF00','#87FF2A','#0048BA','#FF007C','#E936A7']
	color = random.choice(color_list)
	return color

def hash_password(password):
    return hmac.new(str(os.getenv('SECRET_KEY')).encode(),str(password).encode(),digestmod=hashlib.sha512).hexdigest()

@app.route('/favicon.ico/')
def favicon():
    return './favicon.ico'

@app.route('//')
def home():
    return render_template("home.html")

@app.route('/register/',methods=["POST","GET"])
def register():
    if 'user' not in session:
        return render_template('register.html')
    else:
        return redirect(url_for('account'))
    
@app.route('/api/v1/<route>/', methods=["POST"])
def apiv1(route):
    if route == "signup":
        name = request.json.get('name')
        username = request.json.get('username')
        email = request.json.get('email')
        password = request.json.get('password')
        cnf_password = request.json.get('cnfpassword')

        check_username = users.query.filter_by(username=username).first()
        check_email = users.query.filter_by(email=email).first()
        
        if check_email:
            return {
                'response':409,'message':'Email already in use.'
            }
        if email.find("@") == -1:
            return {
                'response':409,'message':'Invalid Email.'
            }
        if check_username:
            return {
                'response':409,'message':'Username already in use.'
            }

        if str(username).rstrip(' ').find(' ') != -1 or str(username).rstrip(' ').find('@') != -1:
            return {
                'response':400,'message':'Username cannot contain @ or spaces in it.'
            }

        if password != cnf_password:
            return {
                'response':304,'message':"Password doesn't match the confirming password."
            }
        
        if '' not in [password,cnf_password,name,username,email]:
            user_id = igen(32)
            registration = users(name=name,username=username,email=email,password=hash_password(password),userid=user_id)
            user_info = info(name=name,userid=user_id, city= '', state= '', country= '', bio= '', skills= '')
            database.session.add(user_info)
            database.session.commit()
            database.session.add(registration)
            database.session.commit()
            system_alert(userid=user_id, message="Welcome to KnowMe.! Seeing this section means your account is successfully verified.")
            system_alert(userid=user_id, message="Update your profile and visit the preview page to see how it looks.")
            return {
                'response':200,'message':"Account Created"
            }
        else:
            return {
                'response':400 ,'message':"Fill all the fields required"
            }

    elif route == "login":
        username = str(request.json.get('username')).rstrip(' ')
        password = request.json.get('password')
        password_hash = hash_password(password)

        if username.find('@') == -1:
            check_username = users.query.filter_by(username=username).first()
        else:
            check_username = users.query.filter_by(email=username).first()

        if check_username != None:
            user_password = check_username.password
            if user_password == password_hash:
                session['user'] = check_username.username
                return {
                    'response':200, 'message':'Logged In!'
                }
            elif user_password != password_hash:
                return {
                    'response':403, 'message':'Username or Password incorrect.'
                }
        else:
            return {
                    'response':403, 'message':'Username or Password incorrect.'
                }
    
    elif route == "account":
        user_data = users.query.filter_by(username=session['user']).first()
        user_info = info.query.filter_by(userid=user_data.userid).first()
        action = request.form.get('action')
        def file_save(file,path):
                file_ext = file.filename.split('.')[-1]
                while True:
                    file_name = igen(16)
                    if not os.path.exists(f'./static/user_uploads/avatar/{file_name}.{file_ext}'):
                        break
                file.save(dst=f"{os.path.abspath(f'static/user_upload/{path}/{file_name}.{file_ext}')}")
                return f"{file_name}.{file_ext}"
        if action == "pictures":
            pfp = request.files.get('profile_picture',None)
            cover = request.files.get('cover_picture',None)
            if pfp != None:
                old_name = user_info.dp
                profile_file_name = file_save(pfp,"avatar")
                user_info.dp = profile_file_name
                if old_name != "default_avatar.png":
                    try:
                        os.remove(path=f"{os.path.abspath(f'static/user_upload/avatar/{old_name}')}")
                    except:
                        pass
            else:
                profile_file_name = user_info.dp
            if cover != None:
                old_name = user_info.cover
                cover_file_name = file_save(cover,"covers")
                user_info.cover = cover_file_name
                if old_name != "original.png":
                    try:
                        os.remove(path=f"{os.path.abspath(f'static/user_upload/covers/{old_name}')}")
                    except:
                        pass
            else:
                cover_file_name = user_info.cover
            database.session.commit()
            return {
                'response':200, 'message':'Updates Successful','profile_picture':profile_file_name,'cover_picture':cover_file_name
                }
        elif action == "UserUpdate":
            full_name = request.form.get('name',None)
            country = request.form.get('country',None)
            state = request.form.get('state',None)
            city = request.form.get('city',None)
            bio = request.form.get('bio',None)
            skills = request.form.get('skills',None)
            title = request.form.get('title',None)
            if '' not in [full_name,country,state,city,bio,skills,title] and None not in [full_name,country,state,city,bio,skills,title]:
                user_data.name = full_name
                user_info.name = full_name
                user_info.country = country
                user_info.city = city
                user_info.state = state
                user_info.bio =bio
                user_info.skills = skills
                user_info.title = title
                user_data.title = title
                database.session.commit()
                database.session.commit()
                return {
                'response':200, 'message':'Updates Successful, Reload Page to see changes'
                }
            else:
                return {
                'response':400, 'message':'Fill all the blanks'
                }
        elif action == "certificate":
            certificate = request.files.get('certificate')
            certificate_name = request.form.get('CertName',None)
            certificate_bio = request.form.get('CertBio',None)
            if certificate == None:
                return {
                    'response':400,'message':'Upload a valid certificate'
                }
            
            if '' not in [certificate_name,certificate.filename,certificate_bio] and None not in [certificate,certificate_bio,certificate_name] and certificate.filename.split('.')[-1] == 'pdf':
                new_filename = file_save(certificate,'certificates')
                add_certificate = certs(userid=user_info.userid, certificate=str(datetime.datetime.now), name=certificate_name, about=certificate_bio, file_name=new_filename)
                database.session.add(add_certificate)
                database.session.commit()
                return{
                    'response':200,'message':'Upload Successful'
                }
            else:
                return{
                    'response':500,'message':'Upload Unsuccessful'
                }
        elif action == "blogUpdate":
            BlogName = request.form.get('name','')
            BlogAbout = request.form.get('about','')
            BlogUrl = request.form.get('url','')

            if '' in [BlogName, BlogAbout, BlogUrl]:
                return {
                    'response':'400',
                    'message':'Fill all the required fields.'
                }
            NewBlog = blogPost(userid=user_data.userid,url=BlogUrl,name=BlogName,about=BlogAbout)
            database.session.add(NewBlog)
            database.session.commit()
            return {
                'response':'200',
                'message':'Blog Updated'
            }
        elif action == "socialUpdate":
            gitlab = request.form.get('Gitlab')
            github = request.form.get('Github')
            twitter = request.form.get('Twitter')
            facebook = request.form.get('Facebook')
            linkedin = request.form.get('Linkedin')
            instagram = request.form.get('Instagram')
            check = sacc.query.filter_by(userid=user_data.userid).first()
            if check == None:
                socialUpdate = sacc(userid=user_data.userid, gitlab=gitlab, github=github, twitter=twitter, facebook=facebook, linkedin=linkedin, instagram=instagram)
                database.session.add(socialUpdate)
                database.session.commit()
            else:
                if gitlab != None:
                    check.gitlab = gitlab
                
                if github != None:
                    check.github = github
                
                if twitter != None:
                    check.twitter = twitter

                if facebook != None:
                    check.facebook = facebook
                
                if linkedin != None:
                    check.linkedin = linkedin
                    
                if instagram != None:
                    check.instagram = instagram

                database.session.commit()

            return redirect(url_for('account'))

        elif action == "passwordUpdate":
            oldPass = request.form.get('oldPass')
            newPass = request.form.get('newPass')
            cnfPass = request.form.get('cnfPass')
            oldHash = hash_password(oldPass)
            if user_data.password == oldHash:
                if newPass == cnfPass:
                    newHash = hash_password(newPass)
                    user_data.password = newHash
                    database.session.commit()
                    return{
                        'response':'200',
                        'message':'Password successfully changed.'
                    }
                else:
                    return{
                        'response':'400',
                        'message':'Confirming Password does not match the new password.'
                    }
            else:
                return{
                        'response':'400',
                        'message':'Incorrect old password.'
                    }
        elif action == "DelAccount":
            password = request.form.get('CnfPassword')
            if password == None:
                return ('Password is Missing', 400)
            password_hash = hash_password(password)
            if user_data.password == password_hash:
                data1 = users.query.filter_by(userid=user_data.userid).first()
                data2 = info.query.filter_by(userid=user_data.userid).first()
                data3 = certs.query.filter_by(userid=user_data.userid).first()
                data4 = sacc.query.filter_by(userid=user_data.userid).first()
                data5 = blogPost.query.filter_by(userid=user_data.userid).first()
                data6 = alerts.query.filter_by(userid=user_data.userid).first()
                if data1 != None:
                    database.session.delete(data1)
                    database.session.commit()

                if data2 != None:
                    database.session.delete(data2)
                    database.session.commit()

                if data3 != None:
                    database.session.delete(data3)
                    database.session.commit()

                if data4 != None:
                    database.session.delete(data4)
                    database.session.commit()
                
                if data5 != None:
                    database.session.delete(data5)
                    database.session.commit()

                if data6 != None:
                    database.session.delete(data6)
                    database.session.commit()

                return redirect(url_for('logout'))
        else:
            return{
                'response':'404',
                'message':'unknown route.'
            }
    elif route == "theme":
        current = request.form.get('current','light')
        theme = current
        if current not in ['dark','light']:
            current = 'light' 
        
        if 'theme' in session:
            session.pop('theme')
           
        session['theme'] = theme 
        return{
            'response':200
        }
            

@app.route('/api/v2/<path>/<opt2>/')
def apiv2(path,opt2=None):
    if path == "delcert" and opt2 != None:
        if 'user' in session:
            user_data = users.query.filter_by(username=session['user']).first()
            request_certificate = certs.query.filter_by(file_name=opt2).first()
            if request_certificate != None:
                if request_certificate.userid == user_data.userid:
                    database.session.delete(request_certificate)
                    database.session.commit()
                    return redirect(url_for('account'))
        else:
            return redirect(url_for('login'))
    elif path == "delBlog":
        user_data = users.query.filter_by(username=session['user']).first()
        request_blog = blogPost.query.filter_by(blogid=opt2).first()
        if request_blog != None:
            if request_blog.userid == user_data.userid:
                database.session.delete(request_blog)
                database.session.commit()
                return redirect(url_for('account'))

@app.route('/login/')
def login():
    if 'user' not in session:
        return render_template('login.html')
    else:
        return redirect(url_for('account'))

@app.route('/logout/')
def logout():
    if 'user' in session:
        session.pop('user')
    return redirect(url_for('home'))

@app.route('/view/<usr>/')
def user(usr):
    user = users.query.filter_by(username=usr).first()
    if user == None:
        return {
            'response':404,
            'message':'user not found'
        }
    user_info = info.query.filter_by(userid=user.userid).first()
    user_certificate = certs.query.filter_by(userid=user.userid).all()
    if len(user_certificate) <= 0:
            user_certificate = None
    blogUpdate = blogPost.query.filter_by(userid=user.userid).all()
    if len(blogUpdate) <= 0:
        blogUpdate = None
        
    social = sacc.query.filter_by(userid=user.userid).first()

    return render_template('view.html',user_info=user_info,bg_image="original.png",skill_set=str(user_info.skills).split(','),user_certificates=user_certificate,blogUpdates=blogUpdate,socials=social)

@app.route('/account/')
def account():
    if 'user' in session:
        user_data = users.query.filter_by(username=session['user']).first()
        user_info = info.query.filter_by(userid=user_data.userid).first()
        alert = alerts.query.filter_by(userid=user_info.userid).all()
        user_certificate = certs.query.filter_by(userid=user_data.userid).all()
        if len(user_certificate) <= 0:
            user_certificate = None
        blogUpdate = blogPost.query.filter_by(userid=user_data.userid).all()
        if len(blogUpdate) <= 0:
            blogUpdate = None
        
        social = sacc.query.filter_by(userid=user_data.userid).first()

        return render_template('account.html',account_info=user_data,user_info=user_info,user_message=alert,user_certificates=user_certificate,blogUpdates=blogUpdate, socials=social)
    else:
        return redirect(url_for('login'))

@app.route("/help/")
def Account_help():
    return render_template('account_help.html')

@app.route('/search/')
def search_user():
    search_term = request.args.get('query')
    all_users = users.query.all()
    relative_search = []
    for user in all_users:
        if user.username.lower().find(search_term.lower()) != -1:
            relative_search.append(user)
            continue
        if user.name.lower().find(search_term.lower()) != -1:
            relative_search.append(user)
    if len(relative_search) < 1:
        relative_search = None
    return render_template('search.html',results=relative_search, query=search_term)

@app.route('/admin/login/gateway/')
def admin_login():
    #TODO
    pass

@app.route('/admin/')
def admin():
    #TODO
    pass

@app.route('/user/database/')
def user_db():
    #TODO
    pass

@app.route('/info/')
def user_info():
    #TODO
    pass

@app.route('/verify/')
def user_verification():
    if 'user' in session:
        user_data = users.query.filter_by(username=session['user']).first()
        verify_id = request.args.get('vid',None)
        print("\n",verify_id,"\n")
        if user_data.verified == False and verify_id == None:
            if verification_links.sent_link.get(session['user']) == None:
                verification_links.sent_link[session['user']] = {'resend_after':datetime.datetime.now() + timedelta(minutes = 10)}
            else:
                if datetime.datetime.now() > verification_links.sent_link[session['user']]['resend_after']:
                    verification_links.sent_link[session['user']]['resend_after'] = datetime.datetime.now() + timedelta(minutes = 10)
                else:
                    return {
                        'response':'425',
                        'message':f'Wait for {int(((verification_links.sent_link[session["user"]]["resend_after"] - datetime.datetime.now()).total_seconds())/60)} minutes before retrying.'
                    }

            msg = Message(
                'Verify Your Account.',
                sender = "Know Me Team",
                recipients=[user_data.email]
            )
            msg.html = f"""<h3>Hi, Welcome to Know Me</h3><p>Please click the button below to verify your account <strong>{user_data.username}</strong><br>If you did not create this account, please don't panic , someone might've miss typed your email instead, you can simply ignore this mail.<br></p><a href="http://192.168.46.95:5000/verify/?vid={user_data.verifyid}" style="display: inline-block; font-weight: 400; line-height: 1.5; color: white; text-align: center; text-decoration: none; vertical-align: middle; cursor: pointer; border-color: #0dcaf0; user-select: none; background-color: #0dcaf0; border: 1px solid transparent; padding: 0.375rem 0.75rem; font-size: 1rem; border-radius: 0.25rem; transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;">Verify Account</a><br><p class="text">In case the button does not work please click the link http://192.168.46.95:5000/verify/?vid={user_data.verifyid}</p></body></html>"""
            mail.send(msg)        
            return {
                "response":200,
                "message":"Sent, verification link."
            }
        elif verify_id != None and user_data.verified == False:
            if user_data.verifyid == verify_id:
                user_data.verified = True
                database.session.commit()
                return redirect(url_for('account'))
            else:
                return('', 403)
        elif user_data.verified == True:
            return {
                'response':403,
                'message':'Already Verified.'
            }
        
        
@app.route('/faq/')
def faq():
    return render_template("faq.html")


if not os.path.isdir('./static/user_upload/'):
    os.mkdir('./static/user_upload')

if not os.path.isdir('./static/user_upload/avatar'):
    os.mkdir('./static/user_upload/avatar')

if not os.path.isdir('./static/user_upload/certificates'):
    os.mkdir('./static/user_upload/certificates')

if not os.path.isdir('./static/user_upload/avatar'):
    os.mkdir('./static/user_upload/avatar')

if not os.path.isfile('./static/user_upload/avatar/default_avatar.png'):
    shutil.copy('./default_avatar.png','./static/user_upload/avatar')

if not os.path.isfile('./static/user_upload/covers/original.png'):
    shutil.copy('./original.png','./static/user_upload/covers/original.png')


if __name__ == '__main__':
    with app.app_context():
        database.create_all()
    app.run(debug=True, host="0.0.0.0")
