function sign_up_validation(){
    
    function password_validation(a,b){
        return a === b;
    }
    const name = document.getElementById('name').value ;
    const email = document.getElementById('email').value ;
    const username = document.getElementById('username').value ;
    const password = document.getElementById('password').value ;
    const conf_password = document.getElementById('cnfpassword').value ;
    
    if(password_validation(password,conf_password)){
        let http = new XMLHttpRequest() ;
        const url = "/api/v1/signup/" ;
        let params = JSON.stringify({'name':name, 'email':email, 'username':username, 'password':password, 'cnfpassword':conf_password}) ;
        http.open('POST',url,true);

        http.setRequestHeader('Content-Type', 'application/json');
        http.onload = function(){
            const response = JSON.parse(this.responseText)
            const response_code = response['response']
            const message = response['message']
            const alert = document.getElementById('message_alert')
            if(response_code > 299){
                alert.className = "alert alert-danger"
                alert.innerText = message
            }
            else if(199 < response_code < 300){
                alert.className = "alert alert-success"
                alert.innerText = message
                window.location = "/login/"
            }
            
        };    
        http.send(params)
    }
    else{
        return
    };
}

function sign_in_validation(){
    
    const username = document.getElementById('username').value ;
    const password = document.getElementById('password').value ;
    let http = new XMLHttpRequest();
    const url = "/api/v1/login/" ;
    const params = JSON.stringify({'username':username, 'password':password});
    http.open('POST',url,true)
    http.setRequestHeader('Content-Type', 'application/json');
    http.onload = function(){
        const response = JSON.parse(this.responseText)
        const response_code = response['response']
        const message = response['message']
        const alert = document.getElementById('message_alert')
        if(response_code > 299){
            alert.className = "alert alert-danger"
            alert.innerText = message
        }
        else if(199 < response_code < 300){
            alert.className = "alert alert-success"
            alert.innerText = message
            window.location = "/"
        }
        
    }
    http.send(params)

}
function theme_changer(){
    const logo = document.getElementById("theme_logo")
    let http = new XMLHttpRequest()
    const url = "/api/v1/theme/"
    let formData = new FormData()
    
    if (logo.ariaLabel == "dark"){
        logo.ariaLabel = "light"
        logo.src = "/static/images/icons/light_mode.svg"
        document.getElementById('theme_css').href = "/static/style/light.css"    
        const gitHubIcon = document.getElementById("githubIcon")
        if(gitHubIcon){
            gitHubIcon.src = "/static/images/icons/gitHub_light.png"
        }
        formData.append("current","light")
    }
    else{
        logo.ariaLabel = "dark"
        logo.src = "/static/images/icons/dark_mode.svg"
        document.getElementById('theme_css').href = "/static/style/dark.css"
        const gitHubIcon = document.getElementById("githubIcon")
        if(gitHubIcon){
            gitHubIcon.src = "/static/images/icons/gitHub_dark.png"
        }
        formData.append("current","dark")
    }
    http.open("POST",url, true)
    http.send(formData)
}
function first_visit(){
    const current_theme = getCookie("THEME");
    if (current_theme != "dark" && current_theme != "light"){
        setCookie("THEME","dark",14)
        theme_changer()
    }
}
function dropdown_icon(){
    const span_element = document.getElementById('navbar-button-span')
    if (span_element.ariaLabel == "collapsed"){
        span_element.className = "nav-button-css navbar-toggled-icon"
        span_element.ariaLabel = "expanded"
    } 
    else
    {
        span_element.className = "nav-button-css navbar-toggler-icon-custom collapse"
        span_element.ariaLabel = "collapsed"
        document.getElementById('navbarSupportedContent').className = "navbar-collapse collapsing"
        document.getElementById('navbarSupportedContent').className = "navbar-collapse collapse"
    }
}

function validate_picture_change(){
    profile_pic = document.getElementById("profile_picture")
    cover_pic = document.getElementById("cover_picture")
    if(profile_pic.value != "" || cover_pic.value != ""){
        const upload_dict = {}
        function validate_picture_file(obj){
            const file_name = obj.value
            const file_name_array = file_name.split('.')
            const ext = file_name_array[file_name_array.length - 1]
            if(["png","jpg","jpeg","gif"].includes(ext)){
                upload_dict[obj.id] = obj.files[0]
            }
        }
        if (profile_pic.value != ""){
            validate_picture_file(profile_pic)
        }
        if(cover_pic.value != ""){
            validate_picture_file(cover_pic)
        }
        let http = new XMLHttpRequest()
        const url = "/api/v1/account/"
        let formData = new FormData()
        formData.append("action","pictures")
        for(file in upload_dict){
            formData.append(file, upload_dict[file])
        }
        
        http.open("POST",url, true)
        http.onload = function(){
            const response = JSON.parse(this.responseText)
            const response_code = response['response']
            const message = response['message']
            const new_pfp = response['profile_picture']
            const new_cover = response['cover_picture']
            const alert_container = document.getElementById('alert-container')
            alert_container.innerHTML = '<div id="message_alert" class="" role="alert"></div>'
            const alert = document.getElementById('message_alert')
            if(response_code > 299){
                alert.className = "alert alert-danger alert-dismissible fade show"
                alert.innerText = message
                alert.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            }
            else if(199 < response_code < 300){
                alert.className = "d-flex alert alert-success alert-dismissible fade show"
                alert.innerText = message
                alert.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            }
            const dp = document.getElementById("dp")
            const cover = document.getElementById("new_cover_display")
            dp.src = "/static/user_upload/avatar/" + new_pfp
            cover.style.backgroundImage = `url(/static/user_upload/covers/${new_cover})`
            profile_pic.value = ""
            cover_pic.value = ""
        }
        http.send(formData)
    }
    else{
        console.log("OwO")
        logo.src = "/static/images/icons/dark_mode.svg"
        current_theme.ariaLabel = "dark"
        current_theme.href = "/static/style/dark.css"
    }
}

function dropdown_icon(){
    const span_element = document.getElementById('navbar-button-span')
    if (span_element.ariaLabel == "collapsed"){
        span_element.className = "nav-button-css navbar-toggled-icon"
        span_element.ariaLabel = "expanded"
    } 
    else{
        span_element.className = "nav-button-css navbar-toggler-icon-custom"
        span_element.ariaLabel = "collapsed"
    }
}

function validate_picture_change(){
    profile_pic = document.getElementById("profile_picture")
    cover_pic = document.getElementById("cover_picture")
    if(profile_pic.value != "" || cover_pic.value != ""){
        const upload_dict = {}
        function validate_picture_file(obj){
            const file_name = obj.value
            const file_name_array = file_name.split('.')
            const ext = file_name_array[file_name_array.length - 1]
            if(["png","jpg","jpeg","gif"].includes(ext)){
                upload_dict[obj.id] = obj.files[0]
            }
        }
        if (profile_pic.value != ""){
            validate_picture_file(profile_pic)
        }
        if(cover_pic.value != ""){
            validate_picture_file(cover_pic)
        }
        let http = new XMLHttpRequest()
        const url = "/api/v1/account/"
        let formData = new FormData()
        formData.append("action","pictures")
        for(file in upload_dict){
            formData.append(file, upload_dict[file])
        }
        
        http.open("POST",url, true)
        http.onload = function(){
            const response = JSON.parse(this.responseText)
            const response_code = response['response']
            const message = response['message']
            const new_pfp = response['profile_picture']
            const new_cover = response['cover_picture']
            const alert_container = document.getElementById('alert-container')
            alert_container.innerHTML = '<div id="message_alert" class="" role="alert"></div>'
            const alert = document.getElementById('message_alert')
            if(response_code > 299){
                alert.className = "alert alert-danger alert-dismissible fade show"
                alert.innerText = message
                alert.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            }
            else if(199 < response_code < 300){
                alert.className = "d-flex alert alert-success alert-dismissible fade show"
                alert.innerText = message
                alert.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            }
            const dp = document.getElementById("dp")
            const cover = document.getElementById("new_cover_display")
            dp.src = "/static/user_upload/avatar/" + new_pfp
            cover.style.backgroundImage = `url(/static/user_upload/covers/${new_cover})`
            profile_pic.value = ""
            cover_pic.value = ""
        }
        http.send(formData)
    }
    else{
        console.log("OwO")
    }
}
function send_verification(){
    let button = document.getElementById('verify_btn')
    console.log(button)
    let http = new XMLHttpRequest()
    const url = "/verify/"
    
    http.onreadystatechange = function(){
        const response = JSON.parse(this.responseText)
        const response_code = response['response']
        const message = response['message']
        const alert_container = document.getElementById('alert-container')
        alert_container.innerHTML = '<div id="message_alert" class="" role="alert"></div>'
        const alert = document.getElementById('message_alert')
        if(response_code > 299){
            alert.className = "alert alert-danger alert-dismissible fade show"
            alert.innerText = message
            alert.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
        }
        else if(199 < response_code < 300){
            alert.className = "d-flex alert alert-success alert-dismissible fade show"
            alert.innerText = message
            alert.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
        }
    }
    http.open("GET",url, true)
    http.send(null)
}

function data_update(){
    const fullName = document.getElementById('fullName').value
    const CountryName = document.getElementById('CountryName').value
    const StateName = document.getElementById('StateName').value
    const CityName = document.getElementById('CityName').value
    const bio = document.getElementById('bioUpdate').value
    const skills = document.getElementById('skills_input').value
    const title = document.getElementById('title_input').value
    const alertDiv = document.getElementById('UserUpdateAlert')

    if(fullName == '' || CountryName == '' || StateName == '' || CityName == '' || bio == '' || skills == '' || title == ''){
        alertDiv.className = "d-flex alert alert-danger "
        alertDiv.innerHTML = "All the fields are required."   
    }
    else{
        
        let http = new XMLHttpRequest()
        const url = "/api/v1/account/"
        let formData = new FormData()
        formData.append('name',fullName)
        formData.append('country',CountryName)
        formData.append('state',StateName)
        formData.append('city',CityName)
        formData.append('bio',bio)
        formData.append('skills',skills)
        formData.append('title',title)
        formData.append("action","UserUpdate")
        http.open("POST",url, true)
        http.onload = function(){
            const response = JSON.parse(this.responseText)
            const response_code = response['response']
            const message = response['message']
            if(response_code > 299){
                alertDiv.className = "alert alert-danger alert-dismissible fade show"
                alertDiv.innerText = message
                alertDiv.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            }
            else if(199 < response_code < 300){
                alertDiv.className = "d-flex alert alert-success alert-dismissible fade show"
                alertDiv.innerText = message
                alertDiv.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            }
        }
        http.send(formData)
    }
}

function certificate_upload(){
    const certificate_file = document.getElementById("CertFile")
    const certificate_name = document.getElementById("CertName").value
    const certificate_bio = document.getElementById("CertBio").value
    const alertDiv = document.getElementById("CertificateUpdateAlert")
    let formData = new FormData()
    function validate_certificate_file(obj){
        const file_name = obj.value
        const file_name_array = file_name.split('.')
        const ext = file_name_array[file_name_array.length - 1]
        if("pdf" == ext){
            formData.append('certificate', obj.files[0])
            return true
        }
        else{
            return false
        }
    }
    if (certificate_file.value == '' || certificate_name == '' || certificate_bio == '' ){
        alertDiv.className = "d-flex alert alert-danger "
        alertDiv.innerHTML = "All the fields are required."
    }
    else{
        if (validate_certificate_file(certificate_file)){
            let http = new XMLHttpRequest()
            const url = "/api/v1/account/"
            
            formData.append('action','certificate')
            formData.append('CertName',certificate_name)
            formData.append('CertBio',certificate_bio)
            http.open("POST",url, true)
            http.onload = function(){
                const response = JSON.parse(this.responseText)
                const response_code = response['response']
                const message = response['message']
                if(response_code > 299){
                    alertDiv.className = "alert alert-danger alert-dismissible fade show"
                    alertDiv.innerText = message
                    alertDiv.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
                }
                else if(199 < response_code < 300){
                    alertDiv.className = "d-flex alert alert-success alert-dismissible fade show"
                    alertDiv.innerText = message
                    alertDiv.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
                }
            }
            http.send(formData)
        }
        
    }
}
function blog_update(){
    const blogName = document.getElementById("BlogName").value
    const aboutBlog = document.getElementById("BlogBio").value
    const blogUrl = document.getElementById("BlogURL").value
    const alertDiv = document.getElementById("BlogUpdateAlert")

    if (blogName == '' || aboutBlog == '' || blogUrl == ''){
        alertDiv.className = "d-flex alert alert-danger "
        alertDiv.innerHTML = "All the fields are required."
    }
    else{
        let http = new XMLHttpRequest()
        const url = "/api/v1/account/"
        let formData = new FormData()
        formData.append('action','blogUpdate')
        formData.append('name',blogName)
        formData.append('about',aboutBlog)
        formData.append('url',blogUrl)
        http.open("POST",url, true)
        http.onload = function(){
            const response = JSON.parse(this.responseText)
            const response_code = response['response']
            const message = response['message']
            if(response_code > 299){
                alertDiv.className = "alert alert-danger alert-dismissible fade show"
                alertDiv.innerText = message
                alertDiv.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            }
            else if(199 < response_code < 300){
                alertDiv.className = "d-flex alert alert-success alert-dismissible fade show"
                alertDiv.innerText = message
                alertDiv.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            }
        }
        http.send(formData)
    }
}
function UpdatePassword(){
    const oldPass = document.getElementById('OldPassword').value
    const newPass = document.getElementById('NewPassword').value
    const cnfPass = document.getElementById('CnfNewPassword').value
    const alertDiv = document.getElementById("PasswordUpdateAlert")
    if(oldPass == '' || newPass == '' || cnfPass == ''){
        alertDiv.className = "d-flex alert alert-danger "
        alertDiv.innerHTML = "All the fields are required."
    }
    else{
        let http = new XMLHttpRequest()
        const url = "/api/v1/account/"
        let formData = new FormData()
        formData.append('action','passwordUpdate')
        formData.append('oldPass',oldPass)
        formData.append('newPass',newPass)
        formData.append('cnfPass',cnfPass)
        http.open("POST",url, true)
        http.onload = function(){
            const response = JSON.parse(this.responseText)
            const response_code = response['response']
            const message = response['message']
            if(response_code > 299){
                alertDiv.className = "alert alert-danger alert-dismissible fade show"
                alertDiv.innerText = message
                alertDiv.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            }
            else if(199 < response_code < 300){
                alertDiv.className = "d-flex alert alert-success alert-dismissible fade show"
                alertDiv.innerText = message
                alertDiv.innerHTML += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            }
        }
        http.send(formData)
    }
}